# tailwind-navbar-react

> A responsive Tailwind navbar component for use with React.

[![NPM](https://img.shields.io/npm/v/tailwind-navbar-react.svg)](https://www.npmjs.com/package/tailwind-navbar-react) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Demo

You can see tailwind-navbar-react in action at https://bbworld1.gitlab.io/tailwind-navbar-react/,
CodeSandbox: https://codesandbox.io/s/tailwind-navbar-react-example-h6ilp

## Install

```bash
npm install --save tailwind-navbar-react
```

## Usage

```jsx
import React, { Component } from 'react';

import { TailwindNavbar } from 'tailwind-navbar-react';
import './tailwind.output.css';

class Example extends Component {
  render () {
    return (
      <TailwindNavbar
        brand={
          <img src="https://media.discordapp.net/attachments/694834406493257762/729067815499202651/matthew_border.png" width="40" height="40" alt="Brand logo" />
        }
        className="py-1"
      >
        <nav>
          <ul className="items-center justify-between pt-4 text-base lg:flex lg:pt-0">
            <li>
              <a className="block px-0 py-3 border-b-2 border-transparent lg:p-4 hover:border-indigo-400" href="/">
                A Link
              </a>
            </li>
          </ul>
        </nav>
      </TailwindNavbar>
    );
  }
}
```

## License

MIT © [bbworld1](https://github.com/bbworld1)
